---
title: Dr. Thomas Wood
subtitle: BM, BCh, PhD
initials: TW
affiliation: Division of Neonatology, Department of Pediatrics
address: University of Washington Medical Center, Box 356320, HSB RR-535
location: Seattle, WA 98195 (USA)
email: tommyrw@uw.edu
image: tw.jpg
sideImage: '/images/cellcounting.jpg'
linkedIn: thomas-wood-35b685a8
web: "http://drragnar.weebly.com/about-me.html"
quote: "Summary Quote here..."
featured: true
weight: 2
---

More Information Here...