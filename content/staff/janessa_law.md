---
title: Dr. Janessa Law
subtitle: MD
initials: JL
affiliation: Division of Neonatology, Department of Pediatrics
address: University of Washington Medical Center, Box 356320, HSB RR-535
location: Seattle, WA 98195 (USA)
email: janessal@uw.edu
image: jl.jpg
sideImage: '/images/cellcounting.jpg'
linkedIn: janessa-law-36a34b162
web: https://www.uwmedicine.org/bios/janessa-law
quote: "Summary Quote here..."
featured: true
weight: 3
---

