---
title: Dr. Sandra Juul
subtitle: MD, PhD
initials: SJ
affiliation: Division of Neonatology, Department of Pediatrics
address: University of Washington Medical Center, Box 356320, HSB RR-535
location: Seattle, WA 98195 (USA)
email: sjuul@uw.edu
image: sj.jpg
sideImage: '/images/cellcounting.jpg'
linkedIn: sandra-juul-9b746247
web: https://www.uwmedicine.org/bios/sandra-juul-ledbetter
quote: "As a neonatologist, I am honored to participate in the care of precious, fragile individuals ranging from extremely preterm infants to term neonates with complex medical or surgical problems. These small patients and their families humble me with their endurance in the face of great adversity. I believe that my role as an academic neonatologist is to work to advance care and improve outcomes for our patient population. My research focuses on neonatal neuroprotection: how to optimize neurodevelopment, protect the newborn brain from injury, and how to repair the injured brain."
featured: true
weight: 1
---

Dr. Sandra "Sunny" Juul, MD, PhD is Division Chief of Neonatology at Seattle Childrens Hospital and the University of Washington. After completing her medical degree, residency, fellowship and two years as an acting Assistant Professor at the University of Washington, Dr. Juul left Seattle in 1991 for the University of Chicago where she began a Ph.D. in Developmental Biology. She then joined the faculty at the University of Florida in 1993, where her Ph.D. work was completed. In 2000, Dr. Juul was successfully recruited back to Seattle. She is board certified in Pediatrics and Neonatology.

Dr. Juul leads a translational neonatal neuroscience research program. The focus is to identify new therapeutic approaches to neonatal brain injury, determine whether they are safe and effective, and if so, bring these new treatments from the laboratory to the bedside. Dr. Juul identified Epo and its receptor in the developing human brain, demonstrated that it is neuroprotective and safe using multiple animal models of neonatal brain injury. Bringing this work from the bench to the bedside, Dr. Juul is currently Principal Investigator of a multicenter randomized controlled trial to determine whether Epo is a safe and effective neuroprotectant for extremely preterm infants. The study, Preterm Epo Neuroprotection (PENUT) is funded by NINDS, and has enrolled 940 babies born between 24-0/7 and 26-6/7 weeks of gestation. These children are currently being followed to two years of age for detailed neurodevelopmental assessments. Dr. Juul is also multi-PI with Dr. Yvonne Wu (UCSF) on the High-Dose Erythropoietin for Asphyxia and Encephalopathy (HEAL) Trial, a randomized controlled trial of Epo neuroprotection for 500 term infants with hypoxic-ischemic encephalopathy (HIE). This multicenter trial will determine whether Epo in addition to therapeutic cooling will improve the outcome for infants with HIE.