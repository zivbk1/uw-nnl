---
title: "UW NNL"
description: "Neonatal Neuroscience Research Laboratory (NNL)"
heroTitle: "Neonatal Neuroscience Laboratory"
heroSubTitle: "University of Washington"
heroImage: "/images/dapicell.jpg"
heroCaption: "Working hard in the laboratory."
ctaButtonText1: Research
ctaButtonLink1: "/research"
ctaButtonText2: Staff
ctaButtonLink2: "/staff"
statHeading: "So Many Experiments!"
statSubtext: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Repellendus repellat laudantium."
stats:
  - name: Reports
    value: "1000"
  - name: Experiments
    value: "20k+"
  - name: Funding to Date
    value: "$100 m"
---
