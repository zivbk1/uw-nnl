const defaultTheme = require("tailwindcss/defaultTheme");

module.exports = {
  theme: {
    screens: {
      'sm': '640px',
      // => @media (min-width: 640px) { ... }

      'md': '768px',
      // => @media (min-width: 768px) { ... }

      'lg': '1024px',
      // => @media (min-width: 1024px) { ... }

      'xl': '1280px',
      // => @media (min-width: 1280px) { ... }

      'xxl': '2500px',
      // => @media (min-width: 2500px) { ... }
    },
    extend: {
      fontFamily: {
        sans: ["Inter var", ...defaultTheme.fontFamily.sans],
      },
      colors: {
        uwGold: '#85754d',
        uwPurple: '#4b2e83',
      },
    },
    typography: {
      default: {
        css: {
          h1: {
            marginTop: '0',
            marginBottom: '0',
          },
          h2: {
            marginTop: '0',
            marginBottom: '0',
          },
          h3: {
            marginTop: '0',
            marginBottom: '0',
          },
        },
      },
      xl: {
        css: {
          h1: {
            marginTop: '0',
            marginBottom: '1.5rem',
          },
          h2: {
            marginTop: '0',
            marginBottom: '1.5rem',
          },
          h3: {
            marginTop: '0',
            marginBottom: '1rem',
          },
        },
      },
    },
  },
  variants: {},
  plugins: [
    require('@tailwindcss/typography'),
    require("@tailwindcss/ui")
  ],
};
