const purgecss = require('@fullhuman/postcss-purgecss')({
  content: [ './hugo_stats.json' ],
  defaultExtractor: (content) => {
      let els = JSON.parse(content).htmlElements;
      return els.tags.concat(els.classes, els.ids);
  }
});
const cssnano = require('cssnano');

module.exports = {
  plugins: [
      require('tailwindcss')('./assets/config/tailwind.config.js'),
      process.env.HUGO_ENVIRONMENT === 'production' ? require('autoprefixer') : null,
      process.env.HUGO_ENVIRONMENT === 'production' ? cssnano({ preset: 'default' }) : null,
      ...(process.env.HUGO_ENVIRONMENT === 'production' ? [ purgecss ] : [])
  ]
};